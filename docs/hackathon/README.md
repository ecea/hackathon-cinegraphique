# Hackathon Cinégraphique
1ère édition du Hackathon Cinégraphique **les 16 et 17 juin 2018**, à [la Paillasse](https://lapaillasse.org) !

Voici l'idée : Deux jours de création de courts films d'animation, sur le thème du végétal. Deux contraintes : utiliser des logiciels libres & créer des films sans paroles. Pour le reste, tout est possible !

Nous mettrons à disposition une banque d'images et de sons, que vous pourrez utiliser en plus de vos propres ressources. Venez même si vous n'avez jamais utilisé de logiciel libre auparavant : de courts ateliers seront organisés pour une prise en main rapide de certains logiciels. L'idée du hackathon est de faire travailler ensemble des gens d'horizons et de compétences différentes, so don't worry n' have fun!

Le format est assez flexible, alors si vous voulez faire une présentation de certains logiciels libres ou participer au support technique, n'hésitez pas à nous écrire en amont.

L'inscription est gratuite.

::: warning Ah oui !  
Pensez à apporter **vos ordinateurs et une souris**, on ne pourra pas en fournir sur place.  
:::   

Un bar sera ouvert, n'hésitez pas à apporter des petites choses à manger et à partager.


[Pour s'inscrire, c'est ici!](https://www.meetup.com/La-Paillasse-Events/events/247985041/)


Si vous souhaitez contribuer, n'hésitez pas à nous envoyer un mail à cinegraphiks at protonmail.com

ou sur [gitlab](https://gitlab.com/ecea/hackathon-cinegraphique)
