
# Programme

### _samedi 16 juin_
- 9h30 - 10h : accueil
- 10h - 10h45 : présentation, tour de table et constitution des groupes
- 11h - 13h : définition des projets, appréhension des outils, banques d'images et de son
- 13h - 14h : repas libre, chacun apporte des plats à partager
- 14h - 14h30 : atelier/présentation  d'un outil libre
- 14h - 19h : session de travail libre
- 19h : apérobilan :unicorn:
- 20h ++ : repas et session nocturne

### _dimanche 17 juin_
- 10h30 : petit dej et retour sur la nocturne
- 11h - 13h : session de travail et ateliers pratiques autour d'outils libres
- 13h - 14h : repas partagé
- 14h - 18h : finalisation des projets
- 18h30 : projection des films réalisés et discussions
