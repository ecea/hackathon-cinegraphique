# Logiciels, tutoriaux & add-ons

Sont ici référencés des tutos et informations intéressantes pour utiliser des logiciels de post-production open source.

## Blender
3D - Editing - Compositing

[WIP - VSE Improved Shortcuts](https://gitlab.com/ecea/blender-vse-shortcuts)

[Blender cheatsheet](https://duckduckgo.com/?q=blender+cheatsheet&t=ffab&ia=cheatsheet&iax=1)

[Forum The Blender Clan](http://blenderclan.tuxfamily.org/html/modules/newbb/)


### Tutoriaux

**Basics VSE**  
[https://www.blender.org/support/tutorials/](https://www.blender.org/support/tutorials/)

[https://www.youtube.com/watch?v=UEIkIrYQYYY&list=PLjyuVPBuorqIhlqZtoIvnAVQ3x18sNev4](https://www.youtube.com/watch?v=UEIkIrYQYYY&list=PLjyuVPBuorqIhlqZtoIvnAVQ3x18sNev4)

[https://www.youtube.com/channel/UC8ND5NKqNTrxGprh-x8FVnw](https://www.youtube.com/channel/UC8ND5NKqNTrxGprh-x8FVnw)

[https://www.youtube.com/watch?v=42AQ_EtRVq4](https://www.youtube.com/watch?v=42AQ_EtRVq4)

### Add-ons pour le VSE (Visual Sequence Editor)

**Montage parametrique**  
[http://www.easy-logging.net/](http://www.easy-logging.net/)
[https://github.com/Nikos-Prinios/easy-logging](https://github.com/Nikos-Prinios/easy-logging)

**VSE Transform Tools**  
[https://github.com/kgeogeo/VSE_Transform_Tools](https://github.com/kgeogeo/VSE_Transform_Tools)

**VSE Quick Functions**  
[https://blenderartists.org/forum/showthread.php?338598-Addon-WIP-VSE-Quick-functions-Snaps-Fades-Zoom-Parenting-Titling-Play-speed](https://blenderartists.org/forum/showthread.php?338598-Addon-WIP-VSE-Quick-functions-Snaps-Fades-Zoom-Parenting-Titling-Play-speed)

**VSE KinoRaw tools**  
[https://blenderartists.org/forum/showthread.php?368242-Addon-Kinoraw-Tools-VSE](https://blenderartists.org/forum/showthread.php?368242-Addon-Kinoraw-Tools-VSE)

**Better rendering**  
[https://github.com/sciactive/pulverize](https://github.com/sciactive/pulverize)

**Velvets???**  
[https://github.com/szaszak/blender_velvets](https://github.com/szaszak/blender_velvets)

**Power Sequencer**  
[https://github.com/GDquest/Blender-power-sequencer](https://github.com/GDquest/Blender-power-sequencer)


Si vous souhaitez contribuer, n'hésitez pas à nous envoyer un mail à cinegraphiks at protonmail.com

## Natron

[Official Website](https://natron.fr/)

## Ardour

[Official Website](https://ardour.org/)

## PureData

[Official Website](https://puredata.info/)

## vvvv

[Official Website](https://vvvv.org/)

## GIMP

[Official Website](https://www.gimp.org/)

## Krita

[Official Website](https://krita.org/en/)

## Processing

[Official Website](https://processing.org/)

## openFrameworks

[Official Website](https://openframeworks.cc/)
