# Ressources

Sont ici présentés l'ensemble des éléments mis à disposition pour le Hackathon.
Ils doivent tous être accessible sous licence Creative Commons, au minimum [BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/2.0/)

Vous pouvez aussi nous envoyer du contenu sur le [hackpad](https://hackmd.io/z8hY4kXXR4uPbPgy5q31EQ#)

## Matériaux

[Nos matériaux](https://cinegraphiks-drive.mycozy.cloud/public?sharecode=eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJzaGFyZSIsImlhdCI6MTUyODM4MTE3NiwiaXNzIjoiY2luZWdyYXBoaWtzLm15Y296eS5jbG91ZCIsInN1YiI6ImVtYWlsIn0.krM-xN-xAS61S4X3lqT_H_9x7rq-2Dbe0iJ-hcp3jntGP8vmWD0BN3n_8zxW7zyvF3pIyMKxdUNgSfn2jmLdng&id=536d9464df46a7ec0b59596d22fa6862)

## Sources glanées

### Textures, 3d

[https://www.blendernation.com/2018/02/28/2gb-free-pbr-textures-leaves-vegetation/](https://www.blendernation.com/2018/02/28/2gb-free-pbr-textures-leaves-vegetation/)

### Images

[Unsplash](https://unsplash.com/) *photo uniquement*

[Pixabay](https://pixabay.com/) *photo et vidéo*

[CreativeCommons](https://search.creativecommons.org/)

[Archive](https://archive.org/index.php)

[Public Domain Review](https://publicdomainreview.org/)

## From Archive - Prelinger Archives
## Prelinger Archives

[4085_Learning_About_Leaves](https://archive.org/details/4085_Learning_About_Leaves)

[6146_Growing_Plants_Without_Soil_01_35_06_08](https://archive.org/details/6146_Growing_Plants_Without_Soil_01_35_06_08)

[0923_Some_Fruits_We_Like_00_01_00_29](https://archive.org/details/0923_Some_Fruits_We_Like_00_01_00_29)

[0756_Garden_Wise_01_00_58_00](https://archive.org/details/0756_Garden_Wise_01_00_58_00)

[6403_HM_Judith_Uncle_Louie_Elk_1948_01_38_28_15](https://archive.org/details/6403_HM_Judith_Uncle_Louie_Elk_1948_01_38_28_15)

[Medicusc1939_13](https://archive.org/details/Medicusc1939_13)



Pour toute question / contribution, n'hésitez pas à nous envoyer un mail à cinegraphiks at protonmail.com
