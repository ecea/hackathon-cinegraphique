module.exports = {
  title: 'Cinégraphiks',
  description: 'Just playing around with films and free softwares',
  themeConfig: {
    sidebar: [
      '/hackathon/',
      '/hackathon/programme',
      '/hackathon/ressources',
      '/hackathon/tutoriaux'
    ]
  }
}
