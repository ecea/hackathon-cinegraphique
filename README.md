Le hackathon est un événement de deux jours lors duquel des participants sont amenés à réaliser des films de courte durée (entre 1 et 5 minutes), sans aucun texte, à partir d'une base de donnée d'image et de sons prédéfinie. Les images et les sons utilisés, ainsi que les films réalisés, seront sous licence Creative Commons.
Le hackathon en lui-même sera introduit par 3 sessions préliminaires, correspondant au workflow Learning - Coding - Producing - Editing.

Le thème de l'édition 2018 sera le végétal.

_The cinegraphic hackathon is a two-days event, where participants are brought to make short movies (between 1 and 5 min), without a word. The movies will be edited from a database preconceived ; thus, movies and the rushes will be published under a Creative Commons licence.
The hackathon itself will be introduced by 3 preliminary sessions, following the workflow Learning - Coding - Producing - Editing._

_The theme of the 2018 edition is "plants"._

# 4 Cinegraphic Hackathon

#### Programme
_samedi 16 juin_  
- 9h30 - 10h : accueil
- 10h - 10h45 : présentation, tour de table et constitution des groupes
- 11h - 13h : définition des projets, appréhension des outils, banques d'images et de son
- 13h - 14h : repas libre, chacun apporte des plats à partager
- 14h - 14h30 : atelier/présentation  d'un outil libre
- 14h - 19h : session de travail libre
- 19h : apérobilan :unicorn:                                                         
- 20h ++ : repas et session nocturne  

_samedi 16 juin_  
- 10h30 : petit dej et retour sur la nocturne
- 11h - 13h : session de travail et ateliers pratiques autour d'outils libres
- 13h - 14h : repas partagé
- 14h - 18h : finalisation des projets
- 18h30 : projection des films réalisés et discussions



# 3 Producing images

La troisième session préliminaire sera consacrée à fournir des premières images pour la base de donnée. Trois ateliers conjoints seront organisés, les participants étant libres de passer de l'un à l'autre au cours de la soirée :
- création d'arbres en 3D (à partir du plug-in sapling)
- création de textures procédurales (écorces en node)
- film de cellules végétales, à partir d'un microscope DIY (avec Astrolabe Expédition) (sous réserve)

_The third and last preliminary session will be focused on procuding images for the database, that will be used in the hackathon itself. Three joint workshops will be organised, in which participants are free to go from one to another:_
_- creation of a tree in 3D (using the sapling plug-in)_
_- creation of procedural textures (bark with nodes)_
_- recording vegetables cells, using a DIY microscope (with Astrolabe Expedition) (with reservations)_

# 2 Coding

[hackpad de la session](https://hackmd.io/GPM_GQopS_G0KvWF80v0Ew#)

_english below_

L'un des avantages de travailler en open source est la possibilité d'implémenter ses propres fonctions, add-on et script facilement. Durant cette session, nous présenterons différentes possibilités d'apporter ses propres modifications au logiciel blender, à travers l'utilisation de python (tous niveaux confondus).

Programme (sous réserve des intervenants) : modification des raccourcis claviers de l'éditeur de séquence vidéo, scripts pour faire du glitch, scripts pour créer un mouvement de feuilles.

_One of the advantages of working with open source software is the possibility to add easily new functions, add-ons and scripts. During this session, we will look at different ways to add its own modifications to blender. We will be using mainly the programming language Python._

_During this session, we will see how to implement your own shortcuts for the video sequence editor (beginners), create a script to move a leaf in a certain direction (intermediate) and make a script to create glitches (advanced)._


# 1 Learning
- [hackpad de la session](https://hackmd.io/OwRgHArApgDAxjAtAEwIYBYKPegRnRXAJigE5EAzMKVMii3MYAZiA===#)
- [Cheatsheets](https://gitlab.com/ecea/blender-cheatsheet)

_english below_

Première session du hackathon sera dévolue à la présentation d'outils libres permettant le montage et la retouche d'images vidéos. La session sera centrée autour du logiciel Blender, mais seront également présentées d'autres solutions : Kdenlive (montage), Natron (Compositing), PureData (logiciel de programmation graphique).

_The first session of the hackathon will be dedicated to open source softwares that can be used to produce and edit movies. The session will be focused on Blender, but other softwares will also be presented: Kdenlive (editing), Natron (compositing), PureData (visual programming language)._

