# from https://www.youtube.com/watch?v=dwnyV5UtKns
# create cubes in sinusoidal form

import bpy
from math import sin

for i in range(50):
    x, y, z = 0, i*2, sin(i)*2
    bpy.ops.mesh.primitive_cube_add(
    location = [x,y,z]
    )
